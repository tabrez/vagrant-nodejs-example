import express from "express"

const app = express()
const port = 3000

app.get(`/`, (_req: express.Request, res: express.Response) =>
  res.send(`Hello NodeJS!`)
)

app.listen(port, () => {
  console.log(`  App is running`)
  console.log(`  Press Ctrl-C to stop\n`)
})
