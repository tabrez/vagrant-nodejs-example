#!/bin/bash
# A bash script meant to install `nvm` and `source ~/.nvm/nvm.sh` to ~/.profile

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash
# echo ". ~/.nvm/nvm.sh" >> ~/.profile
source ~/.nvm/nvm.sh
nvm install --lts
cp -f /vagrant/profile ~/.profile
